<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<form action = "/welcome" method="POST">
    @csrf
    <h1>Buat Document Baru</h1>
    <h3>Sign Up Form</h3>
    <label>First Name :</label>
        <input type="text" name="nama1">
        
        <br><br>
        <label>Last Name : </label>
        <input type="text" name="nama2">

        <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="jk">Male
        <input type="radio" name="jk">Female
        <input type="radio" name="jk">Other

        <br><br>
        <label>Nationality : </label>
        <select>
            <option>Choose Your Nationality</option>
            <option>Indonesia</option>
            <option>Egypt</option>
            <option>Turkey</option>
            <option>Germany</option>
            <option>Canada</option>
        </select>

        <br><br>
        <label>Language Spoken</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br><br>
        <input type="checkbox">English <br><br>
        <input type="checkbox">Arabic <br><br>
        <input type="checkbox">Other

        <br><br>
        <label>Bio: </label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea>

        <br><br>
        <input type="submit" value="Sign Up"> 
    </form>
</body>
</html>